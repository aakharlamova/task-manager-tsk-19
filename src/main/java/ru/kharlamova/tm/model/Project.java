package ru.kharlamova.tm.model;

import ru.kharlamova.tm.api.entity.IWBS;

public class Project extends AbstractEntity implements IWBS {

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

}
