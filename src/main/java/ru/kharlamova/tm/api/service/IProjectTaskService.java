package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskByProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String taskId);

    Project removeProjectById(String userId, String projectId);

}
