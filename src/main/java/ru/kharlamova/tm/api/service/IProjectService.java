package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.api.IService;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String userId, String name, String description);

    Project findOneByName(String userId, String name);

    Project removeOneByName(String userId, String name);

    Project findOneByIndex(String userId, Integer index);

    Project removeOneByIndex(String userId, Integer index);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project updateProjectById(String userId, String id, String name, String description);

    Project startProjectById(String userId, String id);

    Project startProjectByIndex(String userId, Integer index);

    Project startProjectByName(String userId, String name);

    Project finishProjectById(String userId, String id);

    Project finishProjectByIndex(String userId, Integer index);

    Project finishProjectByName(String userId, String name);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

}
