package ru.kharlamova.tm.api.repository;
import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.Task;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Task bindTaskByProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

}
