package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

}
