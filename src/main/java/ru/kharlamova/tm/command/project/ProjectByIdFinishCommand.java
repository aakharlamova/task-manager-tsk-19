package ru.kharlamova.tm.command.project;

import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.util.TerminalUtil;

public class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "Change project status to Complete by project id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishProjectById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
