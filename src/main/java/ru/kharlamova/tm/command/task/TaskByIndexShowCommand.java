package ru.kharlamova.tm.command.task;

import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.util.TerminalUtil;

public class TaskByIndexShowCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-view-by-index";
    }

    @Override
    public String description() {
        return "Show a task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
