package ru.kharlamova.tm.command;

import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
        System.out.println("Start Date: " + task.getDateStart());
        System.out.println("Created: " + task.getCreated());
    }

}
