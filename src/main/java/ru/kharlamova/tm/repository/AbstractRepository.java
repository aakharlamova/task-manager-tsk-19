package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.exception.empty.EmptyIdException;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;
import ru.kharlamova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    public List<E> findAll(final String userId) {
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) result.add(entity);
        }
        return entities;
    }

    public E add(final String userId, E entity) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    public E findById(final String userId, final String id) {
        if ((id == null || id.isEmpty())) throw new EmptyIdException();
        for (final E entity: entities) {
            if (entity == null) continue;
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    public void clear(final String userId) {
        final List<E> result = findAll(userId);
        this.entities.removeAll(result);
    }

    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        remove(userId, entity);
        return entity;
    }

    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        entities.remove(entity);
    }

}
