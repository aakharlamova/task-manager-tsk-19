package ru.kharlamova.tm.service;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.api.IService;
import ru.kharlamova.tm.exception.empty.EmptyIdException;
import ru.kharlamova.tm.exception.empty.EmptyUserIdException;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;
import ru.kharlamova.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    public List<E> findAll(final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    public E add(final String userId, final E entity) {
        if (userId == null) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(userId, entity);
    }

    public E findById(final String userId, final String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    public void clear(final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public E removeById(final String userId, final String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    public void remove(final String userId, final E entity) {
        if (userId == null) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(userId, entity);
    }

}
